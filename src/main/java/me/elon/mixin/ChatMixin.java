package me.elon.mixin;

import me.elon.LeClientMod;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.client.MinecraftClient;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(ClientPlayerEntity.class)
public class ChatMixin {
    /**
     * @author Elon
     * @reason Fuck you Microsoft
     */

    //basically we need to learn how IRC protocol works
    @Overwrite
    public void sendChatMessage(String message, @Nullable Text preview) {
        LeClientMod.LOGGER.info("Intercepted message");

        LeClientMod.LOGGER.info(message);

        String newMessage = String.format("CF<%s> %s", MinecraftClient.getInstance().player.getName().getContent(), message);

        MinecraftClient.getInstance().inGameHud.getChatHud().addMessage(Text.of(newMessage), null, null);
        MinecraftClient.getInstance().inGameHud.getChatHud().addToMessageHistory(message);

        LeClientMod.LOGGER.info(String.valueOf(MinecraftClient.getInstance().inGameHud.getChatHud().getMessageHistory()));
    }
    //translation{key='chat.type.text', args=[literal{Player998}[style={clickEvent=ClickEvent{action=SUGGEST_COMMAND, value='/tell Player998 '},hoverEvent=HoverEvent{action=<action show_entity>, value='net.minecraft.text.HoverEvent$EntityContent@ac2e7a53'},insertion=Player998}], literal{tet}]}
}
//test