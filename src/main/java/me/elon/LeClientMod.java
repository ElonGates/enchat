package me.elon;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LeClientMod implements ClientModInitializer {
    public static final Logger LOGGER = LoggerFactory.getLogger("chatfix");

    public static final Item CHATTY_ITEM = new Item(new FabricItemSettings().group(ItemGroup.MISC));

    public static final String ModID = "";
    public static final String NAME = "";
    public static final String VERSION = "0.0.1";
    
    @Override
    public void onInitializeClient() {


        LOGGER.info("Starting ChatFix Mod");


      //?  Registry.register(Registry.ITEM, new Identifier("chatfix", "chatty_item"), CHATTY_ITEM);
    }
}
